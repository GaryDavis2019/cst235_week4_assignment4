package assignment;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * Servlet implementation class TestServlet
 */

@SuppressWarnings("serial")
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	
    static String PAGE_HEADER = "<html><head><title>helloworlds</title></head><body><outputText>What the What!!!</outputText></body></html>";
    
    static String firstname = "<p>Gary</p>";
    static String lastname = "<p>Davis</p>";

    static String PAGE_FOOTER = "<outputText>What the What!!!</outputText></body></html>";
	

	/**
	 * @see Servlet#init(ServletConfig)
	 */
    @Override
	public void init(ServletConfig config) throws ServletException {
		//super.init(config);
		System.out.println("Test init Method");

	}

	/**
	 * @see Servlet#destroy()
	 */
	@Override
	public void destroy() {
		System.out.println("Test destroy Method");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	//@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		
		System.out.println("Test doGet Method");
		
		//returns from the form
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		request.setAttribute("firstname", firstname);
		request.setAttribute("lastname", lastname);
		//request.getRequestDispatcher("TestResponse.jsp").forward(request, response);
		request.getRequestDispatcher("TestError.jsp").forward(request, response);
		
		writer.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
